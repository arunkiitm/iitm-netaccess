Makes it easy for you to verify netaccess. One click!

The recent changes in IITM network policy made the internet access possible only through netaccess. The verification for the same is a very tedious five click process. This extension will help you do the verification in just one click.
1) Install the extension
2) Fill in all the details in options page. You can got to options page by right clicking the icon or from extensions menu.
3) Click the icon when you want to verify. Sit tight and wait, It might take 5-8 sec to complete the process.

Update:
- Usage will be shown right on top of the icon itself.
- You will be updated when you have 200MB left.
